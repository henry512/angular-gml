import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { ClientsModel } from "../models/clients.model";
import { Endpoints } from "../utils/endpoints.enum";

@Injectable({
    providedIn: 'root'
})
export class ClientsService {

    constructor(private http: HttpClient) {}

    public getAll(): Observable<ClientsModel[]> {
        return this.http.get<ClientsModel[]>(Endpoints.CLIENTS);
    }

    public post(client: ClientsModel): Observable<ClientsModel> {
        return this.http.post<ClientsModel>(Endpoints.CLIENTS, client);
    }

    public put(client: ClientsModel): Observable<ClientsModel> {
        return this.http.put<ClientsModel>(Endpoints.CLIENTS + '/' + client.id, client);
    }

    public delete(id: number): Observable<ClientsModel> {
        return this.http.delete<ClientsModel>(Endpoints.CLIENTS + '/' + id);
    }

}