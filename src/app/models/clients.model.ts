export class ClientsModel {
    constructor(
        public name: string,
        public nit: string,
        public id?: number,
    ) {}
}