import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientsComponent } from './components/clients/clients.component';
import { SocketsComponent } from './components/sockets/sockets.component';
import { DevicesComponent } from './components/devices/devices.component';

const routes: Routes = [
  { path: '', component: ClientsComponent },
  { path: 'clients', component: ClientsComponent },
  { path: 'sockets', component: SocketsComponent },
  { path: 'devices', component: DevicesComponent },
  { path: '**', component: ClientsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
