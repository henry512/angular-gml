import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";

import { ClientsModel } from "../../models/clients.model";
import { ClientsService } from "../../services/clients.service";

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  list_clients: ClientsModel[];
  mode_edit: boolean;
  model_edit: ClientsModel;
  form_client: FormGroup;

  constructor(private _clientsService: ClientsService, private _formBuilder: FormBuilder) { 
    
    this.mode_edit = false;
    this.model_edit = new ClientsModel(null, null, null);
    this.form_client = this._formBuilder.group({
      id: [this.model_edit.id],
      name: [this.model_edit.name],
      nit: [this.model_edit.nit]
    });
  }

  ngOnInit(): void {
    this._clientsService.getAll().subscribe(
      (value) => {
        console.log(value);
        this.list_clients = value;
      },
      (error) => {
        console.log(error);
      },
      () => {
        console.log('complete request...');
      }
    );

    // this.list_clients = this._clientsService.getAllTest();
  }

  add(): void {
    this.form_client.patchValue({
      id: null,
      name: null,
      nit: null
    });
  }

  save(): void {
    let client: ClientsModel = new ClientsModel(this.form_client.value['name'], this.form_client.value['nit']);
    this._clientsService.post(client).subscribe(
      (value) => {
        console.log(value);
        this.list_clients.push(new ClientsModel(value.name, value.nit, value.id));
        this.form_client.patchValue({
          id: null,
          name: null,
          nit: null
        });

        $('#inlineForm').modal('hide')
      }
    );
  }

  update(): void {
    let client: ClientsModel = new ClientsModel(this.form_client.value['name'], this.form_client.value['nit'], this.form_client.value['id']);
    this._clientsService.put(client).subscribe(
      (value) => {
        console.log(value);
        //this.list_clients.push(new ClientsModel(value.name, value.nit, value.id));
        let result = this.list_clients.find(item => item.id == value.id);
        result.name = value.name;
        result.nit = value.nit;

        $('#inlineForm').modal('hide')
      }
    );
  }

  edit(id: number): void {
    this.mode_edit = true;
    let client: ClientsModel = this.list_clients.find(item => item.id == id);
    this.form_client.patchValue({
      id: client.id,
      name: client.name,
      nit: client.nit
    });
  }

  delete(id: number): void {
    this._clientsService.delete(id).subscribe(
      (value) => {
        console.log(value);
        this.list_clients = this.list_clients.filter(item => item.id !== id)
      }
    );
  }

}
